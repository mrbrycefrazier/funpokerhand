object FunPokerHand {
  val DEFAULT = (-1, "")                          //> DEFAULT  : (Int, String) = (-1,"")
  val NUMERICAL_VALUES = Map("J" -> 11, "Q" -> 12, "K" -> 13, "A" -> 14)
                                                  //> NUMERICAL_VALUES  : scala.collection.immutable.Map[String,Int] = Map(J -> 11
                                                  //| , Q -> 12, K -> 13, A -> 14)
  val TEN = 10                              //> TEN  : Int = 10
  val ONE = 1                               //> ONE  : Int = 1
  val TWO = 2                                     //> TWO  : Int = 2
  val THREE = 3                                   //> THREE  : Int = 3
  val FOUR = 4                                    //> FOUR  : Int = 4
  val NINE = 9                              //> NINE  : Int = 9
  def determineRank(hand: String): String = {

    def convertCardRankToNumber_Sorted(cardsInHand: List[(String, Char)]): List[Int] = {
      cardsInHand.map {
        case (cardRank, _) =>
          if (NUMERICAL_VALUES.contains(cardRank)) NUMERICAL_VALUES(cardRank)
          else { try { cardRank.toInt } catch { case e: Exception => throw new Exception("please input correct card rank") } }
      }.toList.sorted
    }

    def determineStraight(cardsAsNumbers: List[Int]): Boolean = { cardsAsNumbers.sliding(TWO).forall { case List(a, b) => (a + ONE == b) } } //note, this works because it is already sorted
    def determineRoyalty(cardsAsNumbers: List[Int]): Boolean = cardsAsNumbers.forall(_ > NINE)
    def determineFlush(cardsInHand: List[(String, Char)]): Boolean = cardsInHand.groupBy { case (_, suit) => suit }.mapValues(_.length).size == ONE

    def hasKindOfN(nDupsByValue: Map[String, Int], n: Int): Boolean = {
      nDupsByValue.values.toList.contains(n)
    }

    def hasTwoPairs(nDupsByValue: Map[String, Int]): Boolean = {
      nDupsByValue.values.count(n => n == TWO) == TWO
    }

    def getRankByN(nDupsByRank: Map[String, Int], order: Int): String = {
      nDupsByRank.find(_._2 == order).getOrElse(DEFAULT)._1.toString
    }

    def convertFromNumberToRank(cardRanksAsNumbers_Sorted: List[Int]): String = {
      val last = cardRanksAsNumbers_Sorted.last
      if (last > TEN) NUMERICAL_VALUES.find(_._2 == cardRanksAsNumbers_Sorted.last).getOrElse(DEFAULT)._1.toString
      else last.toString
    }

    val cardsInHand: List[(String, Char)] = hand.split(" ").map(card => (card.substring(0, card.length - 1), card.last)).toList

    val nDupsByRank: Map[String, Int] = cardsInHand.groupBy { case (rank, _) => rank }.mapValues(_.length)

    val cardRanksAsNumbers: List[Int] = convertCardRankToNumber_Sorted(cardsInHand)

    val isFlush = determineFlush(cardsInHand)
    val isStraight = determineStraight(cardRanksAsNumbers)
    val isRoyalty = determineRoyalty(cardRanksAsNumbers)

    if (isFlush && isStraight && isRoyalty) "Royal Flush"
    else if (isFlush && isStraight) "Straight Flush"
    else if (hasKindOfN(nDupsByRank, FOUR)) "Four of a Kind, " + getRankByN(nDupsByRank, FOUR)
    else if (hasKindOfN(nDupsByRank, THREE) && hasKindOfN(nDupsByRank, TWO)) {
      "Full House, 3:" + getRankByN(nDupsByRank, THREE) + " + 2:" + getRankByN(nDupsByRank, TWO)
    } else if (isFlush) "Flush"
    else if (isStraight) "Straight"
    else if (hasKindOfN(nDupsByRank, THREE)) "Three of a kind, " + getRankByN(nDupsByRank, THREE)
    else if (hasTwoPairs(nDupsByRank)) "Two Pair, " + nDupsByRank.filter(_._2 == TWO).keys.mkString(" + ")
    else if (hasKindOfN(nDupsByRank, TWO)) "One Pair, " + getRankByN(nDupsByRank, TWO)
    else "High Card, " + convertFromNumberToRank(cardRanksAsNumbers)
  }                                               //> determineRank: (hand: String)String

  determineRank("2h 10h 10h 10c 10h")             //> res0: String = Four of a Kind, 10
  determineRank("Jh 10h Kh Ah Qh")                //> res1: String = Royal Flush
  determineRank("9h 10h 8h 7c 6c")                //> res2: String = Straight
  determineRank("9c 10c 8c 7c 6c")                //> res3: String = Straight Flush
  determineRank("Jh 10h Kh Ac Qh")                //> res4: String = Straight
  determineRank("Jh 1h 10h 10c 10h")              //> res5: String = Three of a kind, 10
  determineRank("Jh 1h 10h 10c Jh")               //> res6: String = Two Pair, J + 10
  determineRank("Jh 1h 10h 10c 9h")               //> res7: String = One Pair, 10
  determineRank("Jh Jh 10h 10c 10h")              //> res8: String = Full House, 3:10 + 2:J
  determineRank("3h 1h 4h 2c 9h")                 //> res9: String = High Card, 9
  determineRank("3h 1h 4h 2c Ah")                 //> res10: String = High Card, A
}